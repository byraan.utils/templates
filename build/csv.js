// csv
const csv=require('csv-parser');
// file
const fs=require('fs'),
path=require('path'),
jimp=require('jimp');
// input & output
// const prompts=require('prompts');
// custom global
const commandLineArgs=require('../lib/command-line-args'),
log=require('../lib/log'),
Request=require('../lib/request');
//
cli: {
	commandLineArgs(this, [
		{
			name: 'input',
			alias: 'i',
			type: String,
			defaultValue: [],
			multiple: true
		}, {
			name: 'file',
			alias: 'f',
			type: String
		}, {
			name: 'template',
			alias: 't',
			type: String
		}, {
			name: 'slides',
			alias: 's',
			type: String
		}, {
			name: 'textColor',
			alias: 'c',
			type: String,
			defaultValue: '#126abf'
		}
	]);
}
var request=new Request();
//
if(!fs.existsSync(process.arguments.file)) return log('{{error}} CSV not found');
if(!fs.existsSync(process.arguments.template)) return log('{{error}} HTML template not found');
if(!fs.existsSync(process.arguments.slides)) return log('{{error}} Slides dir not found');
let csvPathData=path.parse(process.arguments.file);
// html
let html=fs.readFileSync(process.arguments.template, 'utf-8');
html=html.split('####');
htmlContent=html[1].trim();
// config
let config=html[0].split('\n');
for(let i=0; i<config.length; i++){
	config[i]=config[i].split('->');
	for(let j=0; j<config[i].length; j++){
		config[i][j]=config[i][j].trim();
		if(j==2){
			let params=new URLSearchParams(config[i][j]);
			config[i][j]={};
			params.forEach((v, k) => {
				config[i][j][k]=JSON.parse(v);
			});
		}
	}
}
// ouput
let csvOutput=`${csvPathData.dir}/${csvPathData.name}`;
if(!fs.existsSync(csvOutput)) fs.mkdirSync(csvOutput);
//
fs.createReadStream(process.arguments.file)
.pipe(csv())
.on('data', (row) => {
	row['Color']=process.arguments.textColor;
	if(process.arguments.input.length){
		if(process.arguments.input.indexOf(row['Name'])<0) return;
	}
	let content=htmlContent;
	// log(`{{info}} ${row['Name']}`);
	Object.keys(row).forEach((key, i) => {
		let value=row[key];
		// console.log(key, value);
		// console.log(new RegExp(`##${key}##`, 'g'), content.match(new RegExp(`##${key}##`, 'g')));
		// log(`{{warn}} Replace ##${key}## => ${value}`);
		content=content.replace(new RegExp(`##${key}##`, 'g'), value);
		// console.log(content);
	});
	//output
	let outputPath=`${csvOutput}/${row['Name']}`;
	if(!fs.existsSync(outputPath)) fs.mkdirSync(outputPath);
	// html
	fs.writeFileSync(`${outputPath}/${row['Name']}.html`, content);
	// slide
	let slidePath=`${process.arguments.slides}/${row['Name']}`;
	// config
	for(let i=0; i<config.length; i++){
		let c=config[i];
		if(c.length<2) continue;
		let input=`${slidePath}/${c[0]}`;
		let output=`${outputPath}/${c[1]}`;
		let args=c[2];
		let outputPathData=path.parse(output);
		if(!fs.existsSync(outputPathData.dir)) fs.mkdirSync(outputPathData.dir, {recursive: true});
		if(['.jpg', '.png'].indexOf(outputPathData.ext)>=0){
			request.await((resolve) => {
				log(`{{info}} [${row['Name']}] ${c[0]} -> ${c[1]}`);
				let image=jimp.read(input);
				if(typeof args=='object' && args.constructor.name=='Object') Object.keys(args).forEach((k, i) => {
					let v=args[k];
					image.then(function(img){
						if(typeof img[k]!='function') return img;
						if(typeof v!='object' && v.constructor.name!='Array') v=[v];
						img[k].apply(img, v);
						return img;
					});
				});
				image.then(function(img){
					img.getBufferAsync(outputPathData.ext=='.jpg'?jimp.MIME_JPEG:outputPathData.ext=='.png'?jimp.MIME_PNG:'').then((buffer) => {
						fs.writeFileSync(output, buffer);
						resolve();
					});
				});
			});
		}
	}
})
.on('end', () => {
});